class AudioPlayer {
    constructor(domElement, library) {
        this.domElement = domElement;
        this.controls = {
            domElement: this.domElement.querySelector(".controls")
        };
        this.coverElement = this.domElement.querySelector(".cover img");
        this.progress = this.domElement.querySelector(".cover .progress");
        this.initialize(library, library[0]);
    }

    initialize(library, currentElement) {
        this.initControls();
        this.initProgressActions();

        this.library = library;
        this.initCurrent(currentElement);

        this.initCover();
        this.current.audio.ontimeupdate = () => { this.updateUI(); };
        this.current.audio.addEventListener('ended', () => this.next());
    }

    initCurrent(currentElement){
        this.current = {
            id: currentElement.id,
            audio: new Audio(currentElement.songPath),
            cover: currentElement.songCover,
            name: currentElement.name,
            artist: currentElement.artist,
            album: currentElement.album,
        };
    }
    
    initCover() {
        this.coverElement.src = this.current.cover;
    }
    
    initControls() {
        this.controls.play = this.domElement.querySelector(".play-button");
        this.controls.previous = this.domElement.querySelector(".previous-button");
        this.controls.next = this.domElement.querySelector(".next-button");
        if (this.controls.play) {
            this.initPlay(this.controls.play);
        }
        if (this.controls.previous) {
            this.initPrevious(this.controls.previous, this.controls.play);
        }
        if (this.controls.next) {
            this.initNext(this.controls.next, this.controls.play);
        }
    }
    
    stop(){
        this.pause();
        this.restartSong();
        this.updateUI();
    }
    
    restartSong() {
        this.current.audio.currentTime = 0;
    }
    
    previous() {
        let currentSong = this.current.id;
        console.log(currentSong, this.library.length);
        if (currentSong === 1) {
            this.stop();
            this.controls.play.querySelector("i").classList.replace("fa-pause", "fa-play");
        } else {
            if (this.current.audio.currentTime >= 2) {
                this.restartSong();
            } else {
                this.stop();
                this.initControls();
                this.initProgressActions();
                this.initCurrent(this.library[currentSong-2]);

                this.initCover();
                this.current.audio.ontimeupdate = () => { this.updateUI(); }
                this.current.audio.addEventListener('ended', () => this.next());
                this.play();
                this.controls.play.querySelector("i").classList.replace("fa-play", "fa-pause");
            }
        }
    }
    
    next() {
        let currentSong = this.current.id;
        const previousPaused = this.current.audio.paused;
        if (this.current.id === this.library.length) {
            this.stop();
            this.initialize(this.library, this.library[0]);
            this.controls.play.querySelector("i").classList.replace("fa-pause", "fa-play");
        } else {
            this.stop();
            this.initControls();
            this.initProgressActions();
            this.initCurrent(this.library[currentSong]);

            this.initCover();
            this.current.audio.ontimeupdate = () => { this.updateUI(); }
            this.current.audio.addEventListener('ended', () => this.next());
            this.play();
            this.controls.play.querySelector("i").classList.replace("fa-play", "fa-pause");
        }
    }
    
    initPrevious(thisDomElement, playDomElement) {
        thisDomElement.onclick = () => {
            const icon = playDomElement.querySelector("i");
            const isPlaying = icon.classList.contains("fa-play");
            if (this.current.audio.currentTime > 2) {
                if (isPlaying) {
                    icon.classList.replace("fa-pause", "fa-play");
                } else{
                    icon.classList.replace("fa-play", "fa-pause");
                }
            }
            this.previous();
        }
    }
    
    initNext(thisDomElement, playDomElement) {
        thisDomElement.onclick = () => {
            const icon = playDomElement.querySelector("i");
            const isPlaying = icon.classList.contains("fa-play");
            if (!isPlaying) {
                icon.classList.replace("fa-play", "fa-pause");
            }
            this.next();
        }
    }

    initPlay(domElement) {
        domElement.onclick = () => {
            const icon = domElement.querySelector("i");
            const isPlaying = icon.classList.contains("fa-pause");
            if (isPlaying) {
                icon.classList.replace("fa-pause", "fa-play");
                this.pause();
            } else {
                icon.classList.replace("fa-play", "fa-pause");
                this.play();
            }
        }
    }

    initProgressActions() {
        const cover = this.domElement.querySelector(".cover");
        cover.onclick = (ev) => {
            const currentX = ev.offsetX;
            const totalX = cover.clientWidth;
            const progress = currentX / totalX;
            this.setCurrentTime(progress);
        }
    }

    play() {
        this.current.audio.play().then().catch(e => console.log(`Error al cargar el archivo: ${e}`));
    }

    pause() {
        this.current.audio.pause();
    }

    updateUI() {
        const total = this.current.audio.duration;
        const elapsed = this.current.audio.currentTime;
        const progress = (elapsed / total) * 90;
        this.progress.style.width = `${progress}%`;
    }

    setCurrentTime(progress) {
        this.current.audio.currentTime = this.current.audio.duration * progress;
    }
}