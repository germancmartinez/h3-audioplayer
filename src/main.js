const library = [
    { id: 1, songPath: "./assets/songs/AnotherWay.mp3", songCover: "./assets/covers/AnotherWay.jpeg", artist: "Gigi D'Agostino", name: "Another Way", album: "L'Amour Toujours" },
    { id: 2, songPath: "./assets/songs/AsYourFriend(LeroyStyles&AfrojackExtendedRemix).mp3", songCover: "./assets/covers/AsYourFriend(LeroyStyles&AfrojackExtendedRemix).png", artist: "Afrojack feat. Chris Brown", name: "As Your Friend (LeroyStyles & Afrojack Extended Mix)", album: "As Your Friend - EP" },
    { id: 3, songPath: "./assets/songs/Billionaire.mp3", songCover: "./assets/covers/Billionaire.jpg", artist: "Travie McCoy feat. Bruno Mars", name: "Billionaire", album: "Billionaire (Deluxe Single)" },
    { id: 4, songPath: "./assets/songs/ICanOnlyImagine.mp3", songCover: "./assets/covers/ICanOnlyImagine.jpg", artist: "David Guetta feat. Chris Brown", name: "I Can Only Imagine (Original Mix)", album: "Nothing But The Beat: Ultimate" },
    { id: 5, songPath: "./assets/songs/InMyMind(AxwellRemix).mp3", songCover: "./assets/covers/InMyMind(AxwellRemix).jpg", artist: "Ivan Gough & Feenixpawl feat Georgi Kay", name: "In My Mind (Axwell Remix)", album: "In My Mind (Axwell Remix)" },
    { id: 6, songPath: "./assets/songs/Memories.mp3", songCover: "./assets/covers/Memories.jpg", artist: "David Guetta feat. Kid Cudi", name: "Memories", album: "One More Love" },
    { id: 7, songPath: "./assets/songs/Trouble.mp3", songCover: "./assets/covers/Trouble.webp", artist: "Coldplay", name: "Trouble", album: "Parachutes" }
];
const playerElement = document.querySelector(".player");
const player = new AudioPlayer(playerElement, library);